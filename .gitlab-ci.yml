# References:
# - https://docs.gitlab.com/ee/ci/yaml
# - https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables

stages:
  - update-ci
  - install
  - test
  - build
  - publish

variables:
  PROJECT_PATH: "$CI_PROJECT_DIR"
  OUTPUT_PATH: "$CI_PROJECT_DIR/artifacts"
  LIBRARY_OUTPUT_PATH: "$CI_PROJECT_DIR/dist/angular-library"

default:
  image: $CI_REGISTRY_IMAGE/ci-node:latest
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules
    policy: pull

install_dependencies:
  stage: install
  tags:
    - docker
  script:
    - yarn install
    - yarn ngcc --properties es2015 --create-ivy-entry-points
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules

test_library:
  stage: test
  tags:
    - docker
  image: $CI_REGISTRY_IMAGE/ci-tests:latest
  script:
    - yarn test:ci
  artifacts:
    name: "tests-and-coverage"
    reports:
      junit:
        - $OUTPUT_PATH/tests/junit-test-results.xml
      cobertura:
        - $OUTPUT_PATH/coverage/cobertura-coverage.xml
  dependencies: []

build_library:
  stage: build
  tags:
    - docker
  script:
    - yarn lint
    - yarn build
  artifacts:
    name: "angular-lib-pipeline"
    paths:
      - $LIBRARY_OUTPUT_PATH

# Publish the library on new tag (could be when merging on master)
# The name of the package must respect naming conventions from GitLab
# - https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention
publish_library:
  stage: publish
  tags:
    - docker
  variables:
    REGISTRY_URI: "gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/npm/"
  before_script:
    - npm config set "@jbardon:registry" "https://$REGISTRY_URI"
    - npm config set "//$REGISTRY_URI:_authToken" "$CI_JOB_TOKEN"
  script:
    - cd $LIBRARY_OUTPUT_PATH

    # Get the published library
    # - yarn config set @jbardon:registry https://gitlab.com/api/v4/packages/npm/
    # - yarn login
    # - yarn add @jbardon/angular-lib-pipeline
    - npm publish
  dependencies:
    - build_library
  only:
    - tags

update_ci_images:
  stage: update-ci
  tags:
    - shell
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - cd $PROJECT_PATH/.ci
  script:
    - docker build --tag $CI_REGISTRY_IMAGE/$STAGE_IMAGE:latest
                   --target $STAGE_IMAGE $PROJECT_PATH/.ci

    - docker push $CI_REGISTRY_IMAGE/$STAGE_IMAGE:latest
  parallel:
    matrix:
      - STAGE_IMAGE: [ci-node, ci-tests]
  only:
    changes:
      - .ci/Dockerfile
